var typed = require('./typed.js'),
    data = typed.data,
    matcher = typed.matcher;

var Maybe = data(a => [Just(a), Nothing]);

var fromJust = matcher({ Just: x => x });

var assert = require('assert');

assert.equal(fromJust(Just(1)), 1);
assert.equal(fromJust(Nothing), undefined);
