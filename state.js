/*globals GLOBAL*/
/*globals Monad*/
/*globals concat, tail*/
exports.install = require('./installer.js').installer(exports);

require('./utils.js').install(GLOBAL);
require('./typed.js').install(GLOBAL);
require('./typeclass.js').install(GLOBAL);

var State = function (f) { this.run = f; };
exports.State = State;

Monad(State, {
    pure: function (x) {
        return new State(function (s) { return { result: x, state: s}; });
    },
    bind: function (f) {
        var self = this;
        return new State(function (s) {
            var o = self.run(s);
            return f(o.result).run(o.state);
        });
    }
});
