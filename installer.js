exports.installer = function (toExport) {
    return function (context) {
        Object.keys(toExport).forEach(function (k) {
            context[k] = toExport[k];
        });
    };
};
