exports.install = require('./installer.js').installer(exports);

var identity = function (x) { return x; };
exports.identity = identity;

var constant = function (x) { return function () { return x; }; };
exports.constant = constant;

var compose = function () {
    var args = Array.prototype.slice.call(arguments);
    return args.reduce(function (f, g) { return function (x) { return f(g(x)); }; });
};
exports.compose = compose;

var tail = function (xs) { return Array.prototype.slice.call(xs, 1); };
exports.tail = tail;

var concat = Array.prototype.concat;
exports.concat = concat;
