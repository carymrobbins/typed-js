/*jshint -W030*/

var assert = require('assert'),
    instances = require('./instances.js'),
        Id = instances.Id,
        Just = instances.Just,
        Nothing = instances.Nothing,
    utils = require('./utils.js'),
        identity = utils.identity,
        constant = utils.constant,
        compose = utils.compose,
    typeclass = require('./typeclass.js'),
        fmap = typeclass.fmap,
        bind = typeclass.bind,
        then = typeclass.then,
    stack = require('./stack.js'),
        pop = stack.pop,
        push = stack.push,
    mdo = require('./do.js').mdo;

var assertDataEqual = function (x, y) {
    if (!x.equals(y)) {
        assert.fail(x, y, '.equals() returned false', '==');
    }
};

var c, f, g, p, q, xs;

// Functor laws.

xs = [Id(1), Just(2), Nothing];

// fmap id = id
f = fmap(identity);
g = identity;
xs.forEach(function (x) { assertDataEqual(f(x), g(x)); });

// fmap (p . q) = (fmap p) . (fmap q)
p = function (x) { return x + 1; };
q = function (x) { return x * 2; };
f = fmap(compose(p, q));
g = compose(fmap(p), fmap(q));
xs.forEach(function (x) { assertDataEqual(f(x), g(x)); });

// Monad laws.

// Left identity: return a >>= f  ≡  f a
xs.forEach(function (mx) {
    f = function (y) { return mx.pure(y + 1); };
    assertDataEqual(mx.pure(1).bind(f), f(1));
});

// Right identity: m >>= return  ≡  m
xs.forEach(function (mx) {
    assertDataEqual(mx.bind(mx.pure), mx);
});

// Associativity: (m >>= f) >>= g  ≡  m >>= (\x -> f x >>= g)
xs.forEach(function (mx) {
    f = function (y) { return mx.pure(y + 1); };
    g = function (y) { return mx.pure(y * 2); };
    assertDataEqual(mx.bind(f).bind(g), mx.bind(function (y) { return f(y).bind(g); }));
});

// State monad.
c = pop.then(pop).then(push(5)).then(push(6));
assert.deepEqual(c.run([1, 2, 3]), { state: [6, 5, 3] });

c = pop.then(pop).then(pop);
assert.deepEqual(c.run([1, 2, 3]), { result: 3, state: [] });

// Do notation.
f = mdo(function () {
    x <- Just(1);
    return Just(x + 1);
});
assertDataEqual(f(), Just(2));

// Generalizes monadic actions.
var addM = mdo(function (x, m) {
    y <- m;
    return m.pure(x + y);
});

assertDataEqual(addM(3, Just(1)), Just(4));
assertDataEqual(addM(3, Nothing), Nothing);
assert.deepEqual(addM(3, [1, 2, 3]), [4, 5, 6]);
