/*global GLOBAL*/
/*global data, matcher*/
/*global Functor, Monad*/
/*global Id, Just, Nothing*/

exports.install = require('./installer.js').installer(exports);
var MODULE = 'INSTANCES';
require('./typed.js').install(GLOBAL);
require('./typeclass.js').install(GLOBAL);

var Identity = data(function (a) { return [Id(a)]; });
exports.Identity = Identity;
exports.Id = Id;

var Maybe = data(function (a) { return [Just(a), Nothing]; });
exports.Maybe = Maybe;
exports.Just = Just;
exports.Nothing = Nothing;

Functor(Identity, {
    map: function (f) { return this.match({ Id: function (x) { return Id(f(x)); } }); }
});

Functor(Maybe, {
    map: function (f) {
        return this.match({
            Just: function (x) { return Just(f(x)); },
            Nothing: function () { return Nothing; }
        });
    }
});

Monad(Identity, {
    pure: function (x) { return Id(x); },
    bind: function (f) {
        return this.match({
            Id: function (x) { return f(x); }
        });
    }
});

Monad(Maybe, {
    pure: function (x) { return Just(x); },
    bind: function (f) {
        return this.match({
            Just: function (x) { return f(x); },
            Nothing: function () { return Nothing; }
        });
    }
});

Monad(Array, {
    pure: function (x) { return [x]; },
    bind: function (f) {
        return Array.prototype.concat.apply([], this.map(f));
    }
});
