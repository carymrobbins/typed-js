exports.install = require('./installer.js').installer(exports);

var esprima = require('esprima');

var data = function (def) {
    var cons = function () { return undefined; },
        ast = esprima.parse('_ = ' + def.toString()).body[0].expression.right,
        adts = ast.body.body[0].argument.elements,
        self = this;
    adts.forEach(function (adt) {
        var adtName, adtArgs;
        if (adt.type === 'CallExpression') {
            adtName = adt.callee.name;
            adtArgs = adt.arguments;
            cons[adtName] = self[adtName] = function () {
                if (adtArgs.length !== arguments.length) {
                    throw Error(adtName + 'expects ' + adtArgs.length +
                                ' argument(s), not ' + arguments.length);
                }
                var result = new cons();
                result._name = adtName;
                result._args = Array.prototype.slice.call(arguments);
                result.toString = function () {
                    return adtName + '(' + this._args.join(', ') + ')';
                };
                result.equals = function (x) {
                    return this.constructor === x.constructor &&
                           this._name === x._name &&
                           !(this._args < x._args || this._args > x._args);
                };
                return result;
            };
        } else {
            adtName = adt.name;
            var x = new cons();
            x._name = adtName;
            x._args = [];
            x.toString = function () { return adtName; };
            x.equals = function (x) {
                return this.constructor === x.constructor &&
                       this._name === x._name &&
                       !(this._args < x._args || this._args > x._args);
            };
            cons[adtName] = self[adtName] = x;
        }
    });
    cons.prototype.match = function (patterns) {
        var p;
        for (p in patterns) {
            if (patterns.hasOwnProperty(p)) {
                if (p === this._name) {
                    return patterns[p].apply(self, this._args);
                }
            }
        }
    };
    return cons;
};
exports.data = data;

var matcher = function (patterns) {
    return function (value) {
        var k;
        for (k in patterns) {
            if (patterns.hasOwnProperty(k)) {
                if (value._name === k) {
                    return patterns[k].apply(this, value._args);
                }
            }
        }
    };
};
exports.matcher = matcher;
