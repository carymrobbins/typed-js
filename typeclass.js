exports.install = require('./installer.js').installer(exports);

var TypeClass = function (names) {
    return function (t, where) {
        var name, f;
        for (name in names) {
            if (names.hasOwnProperty(name)) {
                f = where[name] || names[name];
                if (!(f instanceof Function)) {
                    throw Error('Instance must define ' + name);
                }
                t.prototype[name] = f;
            }
        }
    };
};

var Functor = TypeClass({
    map: undefined
});
exports.Functor = Functor;

var fmap = function (f) { return function (x) { return x.map(f); }; };
exports.fmap = fmap;

var Monad = TypeClass({
    pure: undefined,
    bind: undefined,
    then: function (m) {
        return this.bind(function () {
            return m;
        });
    }
});
exports.Monad = Monad;

var bind = function (mx) { return function (f) { return mx.bind(f); }; };
exports.bind = bind;

var then = function (mx) { return function (my) { return mx.then(my); }; };
exports.then = then;

var liftM = function(f, m1) {
    return m1.bind(function(x1) {
        return m1.pure(f(x1));
    });
};
exports.liftM = liftM;
