/*jshint -W030, evil: true*/
exports.install = require('./installer.js').installer(exports);

require('./instances.js').install(GLOBAL);

var esprima = require('esprima'),
    escodegen = require('escodegen');

var parseFunction = function (f) {
    return esprima.parse('_ = ' + f).body[0].expression.right;
};

var isBindExpr = function (expr) {
    return expr.type === 'ExpressionStatement' &&
           expr.expression.type === 'BinaryExpression' &&
           expr.expression.operator === '<' &&
           expr.expression.right.type === 'UnaryExpression' &&
           expr.expression.right.operator === '-';
};

var getBindExpr = function (expr) {
    return {
        left: expr.expression.left.name,
        right: escodegen.generate(expr.expression.right.argument)
    };
};

var itemgetter = function (item) {
    return function (obj) { return obj[item]; };
};

var mdo = function (f) {
    var func = parseFunction(f),
        params = func.params,
        body = func.body.body,
        bindExpr = getBindExpr(body[0]);
    return Function(
        func.params.map(itemgetter('name')),
        'return ' + bindExpr.right + '.bind(' +
            'function(' + bindExpr.left + ') {' +
                   escodegen.generate(body[1]) +
            '})');
};
mdo.exec = function (f) { return mdo(f)(); };
exports.mdo = mdo;
/*
var addM = mdo(function (x, m) {
    y <- m;
    return m.pure(x + y);
});

var doubleHead = mdo.exec(function () {
    x <- pop;
    return push(x * 2);
});
*/
