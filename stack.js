exports.install = require('./installer.js').installer(exports);

require('./state.js').install(GLOBAL);

var pop = new State(function (xs) {
    return { result: xs[0], state: xs.slice(1) };
});
exports.pop = pop;

var push = function (x) {
    return new State(function (xs) {
        return { state: [].concat([x], xs) };
    });
};
exports.push = push;
